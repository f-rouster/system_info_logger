
#!/bin/bash
# *********************************************************************
# **       Instituto Tecnologico de Costa Rica - Sede Cartago        **
# **                Escuela de Ingenieria en Computacion             **
# **    Introduccion a Sistemas Operativos - Primer Semestre, 2018   **
# **                   Profesor: Esteban Arias Mendez                **
# **                                                                 **
# **    Estudiantes:                                                 **
# **           * Felipe de Jesus Salas Casasola                      **
# **           * Fernando Jose Sanchez Leiva                         **
# **                                                                 **
# **               Script para obtener Info del Sistema              **
# **                                                                 **
# **    Fecha:                                                       **
# **           * 13/06/2018                                          **
# *********************************************************************

echo "        **********************************************************************************"
echo "        ::::::::  :::   :::  ::::::::          ::::::::::: ::::    ::: :::::::::: ::::::::  "
echo "      :+:    :+: :+:   :+: :+:    :+:             :+:     :+:+:   :+: :+:       :+:    :+:  "
echo "     +:+         +:+ +:+  +:+                    +:+     :+:+:+  +:+ +:+       +:+    +:+   "
echo "    +#++:++#++   +#++:   +#++:++#++             +#+     +#+ +:+ +#+ :#::+::#  +#+    +:+    "
echo "          +#+    +#+           +#+             +#+     +#+  +#+#+# +#+       +#+    +#+     "
echo "  #+#    #+#    #+#    #+#    #+#             #+#     #+#   #+#+# #+#       #+#    #+#      "
echo "  ########     ###     ########          ########### ###    #### ###        ########        "
echo "  ************************** Collecting System Information *************************"
echo ""

# Create Logs Directory (to save logs)
{
  mkdir ./system_info_logs &&
  echo " * Directory Created Successfully!"
} || {
  echo " * Directory Already Created!"
}

echo " ** Saving Log Files"
echo " *** Saving Log as HTML"
touch ./system_info_logs/system_information.html
lshw -html >> ./system_info_logs/system_information.html

echo " **** Saving Log as JSON"
touch ./system_info_logs/system_information.json
lshw -json >> ./system_info_logs/system_information.json

echo " ***** Saving Log as XML"
touch ./system_info_logs/system_information.xml
lshw -xml >> ./system_info_logs/system_information.xml

echo " ************************** System Information has been saved *************************"
